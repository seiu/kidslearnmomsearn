<?php
/**
 * Plugin Name: Widget - Signup
 * Plugin URI: http://wiki.seiu.org/wiki/WordPress_signup_widget
 * Description: Embed an already-created signup form into the sidebar - has edits
 * Version: 0.1
 * Author: SEIU New Media
 * Author URI: http://seiu.org
 *
 */

/**
 * Add function to widgets_init that'll load our widget.
 * @since 0.1
 */
add_action( 'widgets_init', 'signup_load_widgets' );

/**
 * Register our widget.
 * 'Signup_Widget' is the widget class used below.
 *
 * @since 0.1
 */
function signup_load_widgets() {
	register_widget( 'Signup_Widget' );
}

/**
 * Signup Widget class.
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update.  Nice!
 *
 * @since 0.1
 */
class Signup_Widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function Signup_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'signup', 'description' => __('Once a sign up form has been created in Blue State Digital (BSD), embed the form into the sidebar using this widget. This widget looks best in the Header Section when including only email address', 'signup') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'signup-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'signup-widget', __('Signup Widget', 'signup'), $widget_ops, $control_ops );
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('title', $instance['title'] );
		$signupurl = $instance['signupurl'];
		$introtext = $instance['introtext'];
		$emailaddress = isset( $instance['emailaddress'] ) ? $instance['emailaddress'] : false;
		$firstname = isset( $instance['firstname'] ) ? $instance['firstname'] : false;
		$lastname = isset( $instance['lastname'] ) ? $instance['lastname'] : false;
		$address = isset( $instance['address'] ) ? $instance['address'] : false;
		$city = isset( $instance['city'] ) ? $instance['city'] : false;
		$state = isset( $instance['state'] ) ? $instance['state'] : false;
		$zip = isset( $instance['zip'] ) ? $instance['zip'] : false;
		$phone = isset( $instance['phone'] ) ? $instance['phone'] : false;


		/* Before widget (defined by themes). */
		echo $before_widget;
		?>
		<div class="widget-signup">
		<h3 class="widget-title"><?php echo $title; ?></h3>
		<span class="widget-introtext"><?php echo $introtext; ?></span>
		<form name="signup" action="<?php echo $signupurl; ?>" method="post" id="signup">
		<?php
			if ( $emailaddress )
				printf( '<div class="widget-formrow formrow-email"><label for="email">Email Address</label> <input id="email" name="email" placeholder="Home Email Address" type="text" /></div>' );
			if ( $firstname )
				printf( '<div class="widget-formrow formrow-firstname"><label for="firstname">First Name</label> <input id="firstname" name="firstname" placeholder="First Name" type="text" /></div>' );
			if ( $lastname )
				printf( '<div class="widget-formrow formrow-lastname"><label for="lastname">Last Name</label> <input id="lastname" name="lastname" placeholder="Last Name" type="text" /></div>' );
			if ( $address )
				printf( '<div class="widget-formrow formrow-address"><label for="addr1">Address Line 1</label> <input id="addr1" name="addr1" placeholder="Address Line 1" type="text" /></div><div class="widget-formrow"><label for="addr2">Address Line 2</label> <input id="addr2" name="addr2" placeholder="Address Line 2" type="text" /></div>' );
			if ( $city )
				printf( '<div class="widget-formrow formrow-city"><label for="city">City</label> <input id="city" name="city" placeholder="City" type="text" /></div>' );
			if ( $state )
				printf( '<div class="widget-formrow formrow-state"><label for="state_cd">State</label>  <select id="state_cd" name="state_cd"><option value=""></option><option value="AA">Armed Forces - Americas</option><option value="AE">Armed Forces - Europe</option><option value="AK">Alaska</option><option value="AL">Alabama</option><option value="AP">Armed Forces - Pacific</option><option value="AR">Arkansas</option><option value="AS">American Samoa</option><option value="AZ">Arizona</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DC">District of Columbia</option><option value="DE">Delaware</option><option value="FL">Florida</option><option value="FM">Federated States of Micronesia</option><option value="GA">Georgia</option><option value="GU">Guam</option><option value="HI">Hawaii</option><option value="IA">Iowa</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="MA">Massachusetts</option><option value="MD">Maryland</option><option value="ME">Maine</option><option value="MH">Marshall Islands</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MO">Missouri</option><option value="MP">Northern Mariana Islands</option><option value="MS">Mississippi</option><option value="MT">Montana</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="NE">Nebraska</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NV">Nevada</option><option value="NY">New York</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="PR">Puerto Rico</option><option value="PW">Palau</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VA">Virginia</option><option value="VI">Virgin Islands</option><option value="VT">Vermont</option><option value="WA">Washington</option><option value="WI">Wisconsin</option><option value="WV">West Virginia</option><option value="WY">Wyoming</option></select></div>' );
			if ( $zip )
				printf( '<div class="widget-formrow formrow-zip"><label for="zip">Zip Code</label> <input id="zip" name="zip" placeholder="Zip Code" type="text" /></div>' );
			if ( $phone )
				printf( '<div class="widget-formrow formrow-phone"><label for="phone">Phone</label><input id="phone" name="phone" placeholder="Phone" type="text" /></div>' );
			
		?>
		<div class="widget-formrow formrow-submit">
			<div id="signupBSD" class="sc_form_item sc_form_button"><button>Send Message</button></div>
		</div>
	
		
		
		</form>
		<script>
    $('input[placeholder], textarea[placeholder]').placeholder();
</script>
		</div> <!-- END .widget-signup -->
		<?php 
		/* After widget (defined by themes). */
		echo $after_widget;
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for image url and link url to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['signupurl'] = strip_tags( $new_instance['signupurl'] );
		$instance['introtext'] = strip_tags( $new_instance['introtext'] );
		$instance['emailaddress'] = isset( $new_instance['emailaddress'] );
		$instance['firstname'] = isset( $new_instance['firstname'] );
		$instance['lastname'] = isset( $new_instance['lastname'] );
		$instance['address'] = isset( $new_instance['address'] );
		$instance['city'] = isset( $new_instance['city'] );
		$instance['state'] = isset( $new_instance['state'] );
		$instance['zip'] = isset( $new_instance['zip'] );
		$instance['phone'] = isset( $new_instance['phone'] );

		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_signupurl() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('', 'signup'), 'signupurl' => __('http://', 'signup'), 'emailaddress' => false, 'firstname' => false, 'lastname' => false, 'address' => false, 'city' => false, 'state' => false, 'zip' => false, 'phone' => false, 'customtf' => __('', 'signup'), 'custommltf' => __('', 'signup'), 'customtfid' => __('', 'signup'), 'custommltfid' => __('', 'signup') );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>

		<!-- Signup URL: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'signupurl' ); ?>"><?php _e('BSD Signup Form URL:', 'signup'); ?></label>
			<input id="<?php echo $this->get_field_id( 'signupurl' ); ?>" name="<?php echo $this->get_field_name( 'signupurl' ); ?>" value="<?php echo $instance['signupurl']; ?>" />
		</p>

		<!-- Intro Text: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'introtext' ); ?>"><?php _e('Intro Text:', 'signup'); ?></label>
			<input id="<?php echo $this->get_field_id( 'introtext' ); ?>" name="<?php echo $this->get_field_name( 'introtext' ); ?>" value="<?php echo $instance['introtext']; ?>" />
		</p>

		<!-- Email Address: Checkbox -->
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['emailaddress'], true ); ?> id="<?php echo $this->get_field_id( 'emailaddress' ); ?>" name="<?php echo $this->get_field_name( 'emailaddress' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'emailaddress' ); ?>"><?php _e('Email Address', 'signup'); ?></label>
		</p>

		<!-- First Name: Checkbox -->
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['firstname'], true ); ?> id="<?php echo $this->get_field_id( 'firstname' ); ?>" name="<?php echo $this->get_field_name( 'firstname' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'firstname' ); ?>"><?php _e('First Name', 'signup'); ?></label>
		</p>

		<!-- Last Name: Checkbox -->
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['lastname'], true ); ?> id="<?php echo $this->get_field_id( 'lastname' ); ?>" name="<?php echo $this->get_field_name( 'lastname' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'lastname' ); ?>"><?php _e('Last Name', 'signup'); ?></label>
		</p>

		<!-- Address: Checkbox -->
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['address'], true ); ?> id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e('Address', 'signup'); ?></label>
		</p>

		<!-- City: Checkbox -->
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['city'], true ); ?> id="<?php echo $this->get_field_id( 'emailaddress' ); ?>" name="<?php echo $this->get_field_name( 'city' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'city' ); ?>"><?php _e('City', 'signup'); ?></label>
		</p>

		<!-- State: Checkbox -->
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['state'], true ); ?> id="<?php echo $this->get_field_id( 'emailaddress' ); ?>" name="<?php echo $this->get_field_name( 'state' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'state' ); ?>"><?php _e('State', 'signup'); ?></label>
		</p>

		<!-- Zip: Checkbox -->
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['zip'], true ); ?> id="<?php echo $this->get_field_id( 'emailaddress' ); ?>" name="<?php echo $this->get_field_name( 'zip' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'zip' ); ?>"><?php _e('Zip', 'signup'); ?></label>
		</p>

		<!-- Phone: Checkbox -->
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['phone'], true ); ?> id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e('Phone', 'signup'); ?></label>
		</p>



	<?php
	}
}

?>